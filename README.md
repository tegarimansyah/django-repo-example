# django-repo-example

## Development Tools

- Using [poetry](https://python-poetry.org/) python package manager
- Using [ruff](https://docs.astral.sh/ruff/) for linter and other tooling for improve quality (black, mypy, bandit, sonar -- To be discussed)
- Using [pre-commit](https://pre-commit.com/) for checking before commit

## Django Development

- [12 Factor app](https://12factor.net/)
- [Two scoops django](https://www.feldroy.com/books/two-scoops-of-django-3-x) -- [example](https://startcodingnow.com/two-scoops-django-config) 

## Git Style

- [Trunk Based Development](https://trunkbaseddevelopment.com/)
    - Small commit but often
- [Conventional Commit](https://www.conventionalcommits.org/en/v1.0.0/)
- [Semantic Versioning](https://semver.org/)

## Deployment

- Gitlab CI/CD
    - Dev push a branch and create a MR
    - **Gitlab runner** run testing when a MR is created
    - Code review by peer
    - Merge to main
    - **Gitlab runner** run build and deploy to staging server
    - Create a new tag to deploy to UAT and production
    - **Gitlab runner** run deploy to UAT or production depends on tag name
    - Just in case we need to do a bug fix, checkout to tag, create a branch and cherry pick a commit (or two) from main branch. Then re-tag the branch. **Gitlab runner** will pick the commit and re-deploy it
    - Just in case we want to merge something that we worry about the stability, then we will use feature flag

## Observability

Ref: https://www.eginnovations.com/blog/the-three-pillars-of-observability-metrics-logs-and-traces

- Logging
- Metrics
- Tracing (optional)